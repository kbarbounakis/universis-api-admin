import { ApplicationService } from "@themost/common";
import LocalScopeAccessConfiguration from './config/scope.access.json';
import {UserReplacer} from "./UserReplacer";
import {ODataModelBuilder} from "@themost/data";
import { AccountReplacer } from "./AccountReplacer";
export class AdminService extends ApplicationService {
    constructor(app) {
        super(app);
        // apply user extensions
        new UserReplacer(app).apply();
        // apply account extensions
        new AccountReplacer(app).apply();
        /**
         * get builder
         * @type ODataModelBuilder
         */
        const builder = app.getService(ODataModelBuilder);
        if (builder != null) {
            // cleanup builder and wait for next call
            builder.clean(true);
            builder.initializeSync();
        }
        // extend universis api scope access configuration
        if (app && app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    // add extra scope access elements
                    const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
                    if (scopeAccess != null) {
                        scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
                    }
                }
            });
        }
    }

}
