import { DataError, HttpForbiddenError } from '@themost/common';
import { DataObjectState } from '@themost/data';
import { SchemaLoaderStrategy } from '@themost/data';
import { cloneDeep } from 'lodash';

export function beforeSave(event, callback) {
  (async () => {
    const context = event.model.context;
    let account;
    // on insert
    if (event.state === DataObjectState.Insert) {
      // get account from target state (non-nullable, non-editable)
      account = event.target.account.id || event.target.account;
    } else {
      // on update, get account from previous state
      if (event.previous == null) {
        throw new DataError(
          'E_PREVIOUS',
          'The previous state of the object cannot be determined.',
          null,
          'SpecialPermission'
        );
      }
      account = event.previous.account;
    }
    // validate limited access
    await validateLimitedAccess(context, account);
  })()
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}

export function afterSave(event, callback) {
  (async () => {
    const context = event.model.context;
    // fetch item after save to cover all states
    const specialPermission = await context
      .model('SpecialPermission')
      .where('id')
      .equal(event.target.id)
      .expand('account')
      .silent()
      .getItem();
    if (
      !(
        specialPermission &&
        specialPermission.account &&
        specialPermission.account.name
      )
    ) {
      throw new DataError();
    }
    // get schema loader
    const schemaLoader = context
      .getApplication()
      .getConfiguration()
      .getStrategy(SchemaLoaderStrategy);
    if (schemaLoader == null) {
      throw new DataError();
    }
    // get model definition of related entity
    const modelDefinition = cloneDeep(
      schemaLoader.getModelDefinition(specialPermission.appliesTo)
    );
    if (modelDefinition == null) {
      throw new DataError();
    }
    // ensure privileges
    modelDefinition.privileges = modelDefinition.privileges || [];
    const targetPrivilege = {
      mask: specialPermission.mask,
      type: 'self',
      account: specialPermission.account.name,
      filter: specialPermission.filter,
    };
    // try to find if privilege exists
    const privilegeExists = modelDefinition.privileges.find((item) => {
      return (
        item.account === targetPrivilege.account &&
        item.mask === targetPrivilege.mask &&
        item.filter === targetPrivilege.filter &&
        item.type === targetPrivilege.type
      );
    });
    // if it does, exit
    if (privilegeExists) {
      return;
    }
    if (event.state === DataObjectState.Insert) {
      // push specialPermission to privileges collection
      modelDefinition.privileges.push(targetPrivilege);
    } else {
      if (event.previous == null) {
        throw new DataError(
          'E_PREVIOUS',
          'The previous state of the object cannot be determined',
          null,
          'SpecialPermission'
        );
      }
      // update privilege definition
      const previousPrivilege = modelDefinition.privileges.find((item) => {
        return (
          item.account === targetPrivilege.account &&
          item.mask === event.previous.mask &&
          item.filter === event.previous.filter &&
          item.type === 'self'
        );
      });
      if (previousPrivilege) {
        Object.assign(previousPrivilege, targetPrivilege);
      }
    }
    // and set model definition
    schemaLoader.setModelDefinition(modelDefinition);
  })()
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}

export function beforeRemove(event, callback) {
  (async () => {
    const context = event.model.context;
    if (event.previous == null) {
      throw new DataError(
        'E_PREVIOUS',
        'The previous state of the object cannot be determined.',
        null,
        'SpecialPermission'
      );
    }
    // get account from previous state
    const account = event.previous.account;
    // validate limited access
    await validateLimitedAccess(context, account);
  })()
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}

export function afterRemove(event, callback) {
  (async () => {
    const context = event.model.context;
    if (event.previous == null) {
      throw new DataError(
        'E_PREVIOUS',
        'The previous state of the object cannot be determined.',
        null,
        'SpecialPermission'
      );
    }
    // get account name
    const account = await context
      .model('Account')
      .where('id')
      .equal(event.previous.account)
      .and('additionalType')
      .equal('Group')
      .select('name')
      .silent()
      .value();
    if (account == null) {
      throw new DataError();
    }
    // get schema loader
    const schemaLoader = context
      .getApplication()
      .getConfiguration()
      .getStrategy(SchemaLoaderStrategy);
    if (schemaLoader == null) {
      throw new DataError();
    }
    // get model definition of related entity
    const modelDefinition = cloneDeep(
      schemaLoader.getModelDefinition(event.previous.appliesTo)
    );
    if (modelDefinition == null) {
      throw new DataError();
    }
    // ensure privileges
    modelDefinition.privileges = modelDefinition.privileges || [];
    const removedPrivilege = {
      account,
      mask: event.previous.mask,
      filter: event.previous.filter,
      type: 'self',
    };
    // try to find if privilege exists
    const privilegeExists = modelDefinition.privileges.findIndex((item) => {
      return (
        item.account === removedPrivilege.account &&
        item.mask === removedPrivilege.mask &&
        item.filter === removedPrivilege.filter &&
        item.type === removedPrivilege.type
      );
    });
    if (privilegeExists === -1) {
      return;
    }
    // remove it from privileges collection
    modelDefinition.privileges.splice(privilegeExists, 1);
    // and set model definition
    schemaLoader.setModelDefinition(modelDefinition);
  })()
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}

async function validateLimitedAccess(context, account) {
  if (context == null || account == null) {
    return;
  }
  // get account additional type
  const accountType = await context
    .model('Account')
    .where('id')
    .equal(account)
    .select('additionalType')
    .silent()
    .value();
  // if it is not a group, exit
  if (accountType !== 'Group') {
    return;
  }
  // get groupAttributes of group
  const group = await context
    .model('Group')
    .where('id')
    .equal(account)
    .expand('groupAttributes')
    .select('id')
    .silent()
    .getItem();
  // if the group is flagged with limited access
  if (
    group &&
    Array.isArray(group.groupAttributes) &&
    group.groupAttributes.find((attribute) => attribute === 'limitedAccess')
  ) {
    // throw forbidden error
    throw new HttpForbiddenError(
      context.__(
        'The group privileges cannot be modified as it has been marked as a limited access core group.'
      )
    );
  }
}
