import { ApplicationService, Args, DataError } from '@themost/common';
import {
  DataObject,
  EdmMapping,
  EdmType,
  ModelClassLoaderStrategy,
  SchemaLoaderStrategy,
} from '@themost/data';
import { cloneDeep } from 'lodash';

export class Account extends DataObject {
  @EdmMapping.func('Permissions', EdmType.CollectionOf('Permission'))
  getPermissions() {
    return this.context
      .model('Permission')
      .where('account')
      .equal(this.getId())
      .cache(false)
      .prepare();
  }

  @EdmMapping.func(
    'SpecialPermissions',
    EdmType.CollectionOf('SpecialPermission')
  )
  getSpecialPermissions() {
    return this.context
      .model('SpecialPermission')
      .where('account')
      .equal(this.getId())
      .cache(false)
      .prepare();
  }

  @EdmMapping.param('items', 'Object', false, true)
  @EdmMapping.action(
    'SpecialPermissions',
    EdmType.CollectionOf('SpecialPermission')
  )
  static setSpecialPermissions(context, items) {
    return context.model('SpecialPermission').save(items);
  }

  @EdmMapping.param('data', 'Object', false, true)
  @EdmMapping.action(
    'copySelfPrivileges',
    EdmType.CollectionOf('SpecialPermission')
  )
  async copySelfPrivileges(data) {
    // validate target account
    Args.notNull(data.account, 'Account in payload');
    const targetAccount = data.account.id || data.account,
      context = this.context;
    // first, prevent unnecessary self copy
    if (targetAccount === this.getId()) {
      throw Object.assign(
        new DataError(
          'E_TARGET_ACCOUNT',
          'The target account may not be the same as the base account.'
        ),
        { statusCode: 409 }
      );
    }
    // then, validate that the target account is a group
    // (group to group copy)
    const targetGroup = await context
      .model('Account')
      .where('id')
      .equal(targetAccount)
      .and('additionalType')
      .equal('Group')
      .select('id', 'name')
      .silent()
      .getItem();
    // if not, throw conflict error
    if (targetGroup == null) {
      throw Object.assign(
        new DataError(
          'E_TARGET_ACCOUNT',
          'The specified target account (Group) cannot be found in the current application context.'
        ),
        { statusCode: 409 }
      );
    }
    // get default permissions of self
    const sourceAccount = context.model('Account').convert(this);
    // note: The function validates administrator permissions,
    // as well as the type of the base account (group).
    const sourcePrivileges = cloneDeep(
      await sourceAccount.getDataModelPermissions()
    );
    if (!(Array.isArray(sourcePrivileges) && sourcePrivileges.length > 0)) {
      return [];
    }
    const sourceSelfPrivileges = sourcePrivileges
      .filter((privilege) => {
        // filter out non-self privileges
        return privilege.type === 'self';
      })
      .map((selfPrivilege) => {
        /*
        // drop access to read-only
        selfPrivilege.mask = PermissionMask.Read;
        */
        // ensure appliesTo
        selfPrivilege.appliesTo = selfPrivilege.entityType;
        // ensure account
        selfPrivilege.account = targetGroup.id;
        // ensure privilege
        selfPrivilege.privilege = selfPrivilege.entityType;
        // ensure parentPrivilege
        selfPrivilege.parentPrivilege = selfPrivilege.parentPrivilege || null;
        // cleanup the title and entityType fields,
        // which are not needed for this operation
        delete selfPrivilege.title;
        delete selfPrivilege.entityType;
        // and return the item
        return selfPrivilege;
      });
    if (
      !(Array.isArray(sourceSelfPrivileges) && sourceSelfPrivileges.length > 0)
    ) {
      return [];
    }
    // get default permissions of target account through model definitions
    // important note: when a SpecialPermission is set, it is always applied to the model definition
    // so getDataModelPermissions() actually combines data from SpecialPermissions and system permissions
    const targetPrivileges = await context
      .model('Account')
      .convert(targetAccount)
      .getDataModelPermissions();
    if (!Array.isArray(targetPrivileges)) {
      return [];
    }
    const targetSelfPrivileges = targetPrivileges.filter((targetPrivilege) => {
      // filter out non-self privileges
      return targetPrivilege.type === 'self';
    });
    // construct the final collection of items for SpecialPermissions
    const specialPermissionCollection = sourceSelfPrivileges.filter(
      (sourcePrivilege) => {
        const alreadyAssigned = targetSelfPrivileges.findIndex(
          (targetPrivilege) => {
            return (
              // match privilege account
              targetPrivilege.account === targetGroup.name &&
              // match privilege filter
              targetPrivilege.filter === sourcePrivilege.filter &&
              // if mask is ge than read-only for the specific filter, exclude item
              targetPrivilege.mask >= sourcePrivilege.mask &&
              targetPrivilege.entityType === sourcePrivilege.appliesTo
            );
          }
        );
        // return the privileges that are not already assigned to the group
        return alreadyAssigned === -1;
      }
    );
    // if the final collection is empty, exit
    if (specialPermissionCollection.length === 0) {
      return [];
    }
    // save items
    // note: listeners validate the limitedAccess group attribute
    // and set model definitions on-the-fly
    const result = await context
      .model('SpecialPermission')
      .save(specialPermissionCollection);
    // and finally return the result
    return result;
  }

  @EdmMapping.func('ModelPermissions')
  async getDataModelPermissions() {
    const context = this.context;
    // guard this endpoint from non Administrator users
    const contextUser = context.user;
    if (contextUser == null) {
      throw new HttpForbiddenError();
    }
    // get user groups
    const user = await context
      .model('User')
      .where('name')
      .equal(contextUser.name)
      .select('id')
      .expand('groups')
      .silent()
      .getItem();
    if (!(user && Array.isArray(user.groups))) {
      throw new HttpForbiddenError();
    }
    // ensure that the user is an admin
    const isAdministrator = user.groups.find(
      (group) =>
        group.name === 'Administrators'
    );
    if (!isAdministrator) {
      throw new HttpForbiddenError();
    }
    // get account (group) name
    const accountName = await context
      .model('Account')
      .where('id')
      .equal(this.getId())
      .and('additionalType')
      .equal('Group')
      .select('name')
      .silent()
      .value();
    if (!accountName) {
      return [];
    }
    // get schema loader
    const schemaLoader = context
      .getApplication()
      .getConfiguration()
      .getStrategy(SchemaLoaderStrategy);
    if (schemaLoader == null) {
      throw new DataError();
    }
    // construct account privileges
    return (
      schemaLoader
        // get model names
        .getModels()
        .map((modelName) => {
          // convert them to data models
          return context.model(modelName);
        })
        .filter((dataModel) => {
          // filter those that contain privileges
          return (
            Array.isArray(dataModel.privileges) &&
            dataModel.privileges.length > 0
          );
        })
        .map((dataModel) => {
          // map to privileges array
          return dataModel.privileges
            .filter((item) => {
              return item.account === '*' || item.account === accountName;
            })
            .map((privilege) => {
              // assign entity and title info
              Object.assign(privilege, {
                entityType: dataModel.name,
                title: dataModel.title || dataModel.name,
              });
              return privilege;
            });
        })
        // and finally flatten to a single array
        .flat()
        // keep specific account privileges on top
        .sort((accumulator, next) =>
          accumulator.account < next.account ? 1 : -1
        )
    );
  }

  @EdmMapping.func('DefaultPermissions')
  async getAllDefaultPermissions() {
    const context = this.context;
    const self = context.model('Account').convert(this.getId());
    // get permissions from model definitions
    const dataModelPermissions = cloneDeep(
      await self.getDataModelPermissions()
    );
    // get special permissions
    const specialPermissions = await context
      .model('SpecialPermission')
      .where('account')
      .equal(self.id)
      .silent()
      .getAllItems();
    // if none exist, return the default data model permissions
    if (!(Array.isArray(specialPermissions) && specialPermissions.length > 0)) {
      return dataModelPermissions;
    }
    // exclude special permissions from default permissions collection
    return dataModelPermissions.filter((permission) => {
      const isSpecialPermission = specialPermissions.findIndex(
        (specialPermission) => {
          return (
            specialPermission.account === self.id &&
            specialPermission.appliesTo === permission.entityType &&
            specialPermission.filter === permission.filter &&
            specialPermission.mask === permission.mask &&
            permission.type === 'self'
          );
        }
      );
      return isSpecialPermission === -1;
    });
  }
}

export class AccountReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // get schema loader
    const schemaLoader = this.getApplication()
      .getConfiguration()
      .getStrategy(SchemaLoaderStrategy);
    // get model definition
    const model = schemaLoader.getModelDefinition('Account');
    // get model class
    const loader = this.getApplication()
      .getConfiguration()
      .getStrategy(ModelClassLoaderStrategy);
    const AccountClass = loader.resolve(model);
    // set account extensions
    AccountClass.prototype.getPermissions = Account.prototype.getPermissions;
    AccountClass.prototype.getSpecialPermissions =
      Account.prototype.getSpecialPermissions;
    AccountClass.prototype.copySelfPrivileges =
      Account.prototype.copySelfPrivileges;
    AccountClass.prototype.getDataModelPermissions =
      Account.prototype.getDataModelPermissions;
    AccountClass.prototype.getAllDefaultPermissions =
      Account.prototype.getAllDefaultPermissions;
    AccountClass.setSpecialPermissions = Account.setSpecialPermissions;
  }
}
