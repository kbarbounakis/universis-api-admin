import {ApplicationService, Args, DataNotFoundError, HttpForbiddenError} from "@themost/common";
import {
    DataPermissionEventListener,
    EdmMapping,
    EdmType,
    ModelClassLoaderStrategy,
    SchemaLoaderStrategy
} from "@themost/data";
import {promisify} from 'util';

export class User {
    @EdmMapping.func('Permissions', EdmType.CollectionOf('Permission'))
    static getPermissions(context) {
        return context.model('Permission').asQueryable().cache(false).prepare();
    }

    @EdmMapping.param('entityType', EdmType.EdmString, false, false)
    @EdmMapping.action('DefaultPermissions')
    static async getDefaultPermissions(context, entityType) {
        // guard this endpoint from non Administrator users
        const contextUser = context.user;
        if (contextUser == null) {
            throw new HttpForbiddenError();
        }
        // get user groups
        const user = await context.model('User')
            .where('name').equal(contextUser.name)
            .select('id')
            .expand('groups')
            .silent()
            .getItem();
        if (!(user && Array.isArray(user.groups))) {
            throw new HttpForbiddenError();
        }
        // ensure that the user is an admin
        const isAdministrator = user.groups.find(group => (group.name === 'Administrators' || group.alternateName === 'Administrators'));
        if (!isAdministrator) {
            throw new HttpForbiddenError();
        }
        // validate model param
        Args.notNull(entityType, 'Entity type');
        // validate data model
        const dataModel = context.model(entityType);
        if (dataModel == null) {
            throw new DataNotFoundError(`The requested entity type ${entityType} cannot be found in the current application context.`);
        }
        // and return the default data model privileges
        return Array.isArray(dataModel.privileges) ?
            dataModel.privileges.filter((privilege) => privilege.account).sort((accumulator, next) => accumulator.type > next.type ? 1 : -1)
            : [];
    }

    @EdmMapping.param('items', 'Object', false, true)
    @EdmMapping.action('Permissions', 'Object')
    static setPermissions(context, items) {
        return context.model('Permission').save(items);
    }

    @EdmMapping.param('template', 'Object', false, true)
    @EdmMapping.action('HasPermission', 'Object')
    async hasPermission(template) {
        Args.notString(template.model, 'Target model');
        const validator = new DataPermissionEventListener();
        // noinspection JSUnresolvedFunction
        const finalTemplate = Object.assign({}, template);
        // convert model to DataModel
        finalTemplate.model = this.context.model(finalTemplate.model);
        const validateAsync = promisify(validator.validate).bind(validator);
        await validateAsync(Object.assign(finalTemplate, { throwError: false}));
        return {
            value: finalTemplate.result
        };
    }
}

export class UserReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('User');
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        const UserClass = loader.resolve(model);
        // set user extensions
        UserClass.getPermissions = User.getPermissions;
        UserClass.setPermissions = User.setPermissions;
        UserClass.getDefaultPermissions = User.getDefaultPermissions;
        UserClass.prototype.hasPermission = User.prototype.hasPermission;
    }

}
