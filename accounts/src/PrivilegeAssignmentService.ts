// @ts-ignore
import { ApplicationService, TraceUtils } from '@themost/common';
import {
  SchemaLoaderStrategy,
  DataConfigurationStrategy,
  DataModel,
  PrivilegeType,
  DataModelPrivilege,
  DataField,
  DataContext,
}
// @ts-ignore
from '@themost/data';
import { cloneDeep } from 'lodash';

export declare interface PrivilegeExtensionConfiguration {
  accounts: AccountPrivilegeConfiguration[];
  excludeEntities?: string[];
  assignmentStrategy?: string;
  combineWithSourcePrivileges?: boolean;
}

export declare interface SourceAccountPrivilegeConfiguration {
  name: string;
  method: string;
}

export declare interface TargetAccountPrivilegeConfiguration
  extends SourceAccountPrivilegeConfiguration {
  methodEntityType: string;
  mask?: number;
}

export declare interface AccountPrivilegeConfiguration {
  from: SourceAccountPrivilegeConfiguration;
  to: TargetAccountPrivilegeConfiguration;
}

export enum PermissionAssignmentStrategy {
  FirstMatch = 'firstMatch',
  Exhaustive = 'exhaustive',
}

/*
    Example configuration:
    "admin": {
      "upgradeAccounts": {
        "accounts": [
          {
            "from": {
              "name": "Registrar",
              "method": "departments()"
            },
            "to": {
              "name": "RegistrarAssistants",
              "method": "studyPrograms()",
              "methodEntityType": "StudyProgram"
            }
          }
        ]
      }
    }
*/
export class PrivilegeAssignmentService extends ApplicationService {
  public configuration: PrivilegeExtensionConfiguration;
  private dataConfiguration: DataConfigurationStrategy;
  private context: DataContext;
  constructor(app: any) {
    // call super constructor
    super(app);
    // get service configuration
    this.configuration = Object.assign(
      {
        assignmentStrategy: PermissionAssignmentStrategy.FirstMatch,
        combineWithSourcePrivileges: true,
      },
      app
        .getConfiguration()
        .getSourceAt('settings/universis/admin/upgradeAccounts')
    );
    // create a context
    this.context = app.createContext();
    // the the data configuration instance
    this.dataConfiguration = app
      .getConfiguration()
      .getStrategy(DataConfigurationStrategy);
    // install service
    this.install(app);
    // finalize context
    this.context.finalize((err) => {
      if (err) {
        TraceUtils.error(err);
      }
    });
  }

  install(app: any): void {
    try {
      const applicationConfiguration = app.getConfiguration();
      const schemaLoader: SchemaLoaderStrategy =
        applicationConfiguration.getStrategy(SchemaLoaderStrategy);
      const modelNames: string[] = schemaLoader.getModels();
      const excludeEntities = this.configuration.excludeEntities || [];
      modelNames
        .filter((modelName: string) => {
          // exclude entities
          return !excludeEntities.includes(modelName);
        })
        .map((modelName: string) => {
          // map names to data models (cloned)
          return cloneDeep(
            this.dataConfiguration.getModelDefinition(modelName)
          );
        })
        .forEach((model: DataModel) => {
          let updateModelDefinition = false;
          for (const accountData of this.configuration.accounts) {
            // get source and target accounts
            const sourceAccount: SourceAccountPrivilegeConfiguration =
              accountData.from;
            const targetAccount: TargetAccountPrivilegeConfiguration =
              accountData.to;
            // try to find the source privileges collection
            const sourcePrivileges: DataModelPrivilege[] = (
              model.privileges || []
            ).filter((privilege: DataModelPrivilege) => {
              return (
                // privileges of type self
                privilege.type === PrivilegeType.Self &&
                // that refer to the source account
                privilege.account === sourceAccount.name &&
                // and are related to the source method (e.g departments())
                privilege.filter &&
                privilege.filter.includes(sourceAccount.method)
              );
            });
            // if no source privileges exist
            if (sourcePrivileges.length === 0) {
              // continue
              continue;
            }
            // ensure base model privileges
            model.privileges = model.privileges || [];
            // if the source and target account methods are the same, this is a copy process
            // the privileges are already well defined, so no need to generate any
            if (sourceAccount.method === targetAccount.method) {
              // if a specific mask is defined in the configuration
              if (typeof targetAccount.mask === 'number') {
                // check if privilege is already globally given to the wildcard (all accounts)
                const privilegeForWildcard = model.privileges.find(
                  (item: DataModelPrivilege) => {
                    return (
                      item.mask === targetAccount.mask &&
                      item.type === PrivilegeType.Global &&
                      item.account === '*'
                    );
                  }
                );
                // and if it is, continue
                if (privilegeForWildcard) {
                  continue;
                }
                // try to find if a privilege with that mask exists
                // important: if it does not, use only one (the first) privilege to avoid conflicts
                const privilege =
                  sourcePrivileges.find((item: DataModelPrivilege) => {
                    item.mask === targetAccount.mask;
                  }) || sourcePrivileges[0];
                // push the privilege
                model.privileges.push({
                  mask: targetAccount.mask,
                  type: privilege.type,
                  account: targetAccount.name,
                  filter: privilege.filter,
                });
              } else {
                for (const sourcePrivilege of sourcePrivileges) {
                  // push the new privilege for the target account
                  model.privileges.push({
                    mask: sourcePrivilege.mask,
                    type: sourcePrivilege.type,
                    account: targetAccount.name,
                    filter: sourcePrivilege.filter,
                  });
                }
              }
              // set update flag
              updateModelDefinition = true;
            } else {
              // find available privilege paths based on target account method entity type
              const targetPrivileges: string[] = this.generatePrivilegePaths(
                model,
                targetAccount
              ).map((item: { field: DataField; path: string }) => {
                return `${item.path} eq ${targetAccount.method}`;
              });
              // if there are none, continue
              if (targetPrivileges.length === 0) {
                continue;
              }
              // enumerate source privileges
              for (const sourcePrivilege of sourcePrivileges) {
                if (
                  sourcePrivilege.filter!.includes(' or ') &&
                  this.configuration.combineWithSourcePrivileges
                ) {
                  // preserve the other privileges (e.g owner eq me())
                  const preservePrivileges = sourcePrivilege
                    .filter!.split(' or ')
                    .map((item: string) => item.trim())
                    .filter(
                      (item: string) => !item.includes(sourceAccount.method)
                    );
                  // push the new privilege
                  model.privileges.push({
                    mask: sourcePrivilege.mask,
                    type: PrivilegeType.Self,
                    account: targetAccount.name,
                    filter: [...targetPrivileges, ...preservePrivileges].join(
                      ' or '
                    ),
                  });
                } else {
                  // push the new privilege
                  model.privileges.push({
                    mask: sourcePrivilege.mask,
                    type: PrivilegeType.Self,
                    account: targetAccount.name,
                    filter: targetPrivileges.join(' or '),
                  });
                }
              }
              // set update flag
              updateModelDefinition = true;
            }
          }
          if (updateModelDefinition) {
            // get the original model definition to be safe
            const originalModelDefinition =
              this.dataConfiguration.getModelDefinition(model.name);
            // apply just the privileges
            originalModelDefinition.privileges = model.privileges!.reduce(
              (accumulator, currentPrivilege) => {
                const existingPrivilege = accumulator.find(
                  (item: any) =>
                    this.configuration.accounts.find(
                      (account) => account.to.name === currentPrivilege.account
                    ) &&
                    item.type === PrivilegeType.Self &&
                    item.filter === currentPrivilege.filter &&
                    item.account === currentPrivilege.account
                );
                if (existingPrivilege) {
                  // combine masks of privileges with the same filter
                  existingPrivilege.mask =
                    // tslint:disable-next-line:no-bitwise
                    existingPrivilege.mask | currentPrivilege.mask;
                } else {
                  accumulator.push(currentPrivilege);
                }
                return accumulator;
              },
              []
            );
            // and update the model definition
            this.dataConfiguration.setModelDefinition(originalModelDefinition);
          }
        });
      TraceUtils.info(
        `Services: ${this.constructor.name} service has been successfully installed.`
      );
    } catch (err) {
      TraceUtils.error(err);
      throw err;
    }
  }

  private getIteratableFields(
    model: DataModel
  ): { field: DataField; path: string }[] {
    const collection = ((model && model.fields) || [])
      .map((field: DataField) => {
        const dataModel = cloneDeep(
          this.dataConfiguration.getModelDefinition(field.type!)
        );
        if (dataModel) {
          return dataModel.fields
            .filter((item: DataField) => {
              return (
                !item.many &&
                (item.mapping == null ||
                  item.mapping.associationType !== 'junction')
              );
            })
            .map((item: DataField) => {
              return {
                field: item,
                path: `${field.name}/${item.name}`,
              };
            });
        }
      })
      .filter((item) => item && item.length > 0);
    if (collection.length === 0) {
      return [];
    }
    const result: { field: DataField; path: string }[] = [];
    collection.forEach((modelFields: { field: DataField; path: string }[]) => {
      modelFields.forEach((dataField: { field: DataField; path: string }) => {
        const exists = result.find((item) => {
          return (
            item.field.model === dataField.field.model &&
            item.field.name === dataField.field.name
          );
        });
        if (!exists) {
          result.push(dataField);
        }
      });
    });
    return result;
  }

  private getFieldWithType(
    entityType: string,
    model: DataModel
  ): DataField | undefined {
    return ((model && model.fields) || []).find((field: DataField) => {
      return (
        field.type === entityType &&
        !field.many &&
        (field.mapping == null || field.mapping.associationType !== 'junction')
      );
    });
  }

  private getIndexOfFieldWithType(
    entityType: string,
    model: DataModel
  ): number {
    return ((model && model.fields) || []).findIndex((field: DataField) => {
      return (
        field.type === entityType &&
        !field.many &&
        (field.mapping == null || field.mapping.associationType !== 'junction')
      );
    });
  }

  private populateFieldsFromInheritance(model: any) {
    if (model == null) {
      return;
    }
    if (model.inherits) {
      let nextIheritedModel: any = this.dataConfiguration.getModelDefinition(
        model.inherits
      );
      do {
        nextIheritedModel.fields.forEach((field: DataField) => {
          const exists = model.fields.find((baseField: DataField) => {
            return baseField.name === field.name;
          });
          if (!exists) {
            model.fields.unshift(field);
          }
        });
        nextIheritedModel = this.dataConfiguration.getModelDefinition(
          nextIheritedModel.inherits
        );
      } while (nextIheritedModel);
    }
    if (model.implements) {
      let nextImplementedModel: any = this.dataConfiguration.getModelDefinition(
        model.implements
      );
      do {
        nextImplementedModel.fields.forEach((field: DataField) => {
          const exists = model.fields.find((baseField: DataField) => {
            return baseField.name === field.name;
          });
          if (!exists) {
            model.fields.unshift(field);
          }
        });
        nextImplementedModel = this.dataConfiguration.getModelDefinition(
          nextImplementedModel.implements
        );
      } while (nextImplementedModel);
    }
    return model;
  }

  public generatePrivilegePaths(
    model: DataModel,
    options: TargetAccountPrivilegeConfiguration
  ) {
    // get target entity type
    const targetEntityType = options.methodEntityType;
    const result: {
      field: DataField;
      path: string;
    }[] = [];
    // populate model fields
    this.populateFieldsFromInheritance(model);
    if (model.fields.length === 0) {
      return result;
    }
    // before everything, check the base model
    if (targetEntityType === model.name) {
      const contextModel = this.context.model(model.name);
      const primaryKeyField = contextModel.getAttribute(
        contextModel.primaryKey
      );
      if (primaryKeyField) {
        result.push({
          field: primaryKeyField,
          path: primaryKeyField.name,
        });
      }
      return result;
    }
    // check if the field of specific type exists in base model
    const baseModelIndex = this.getIndexOfFieldWithType(
      targetEntityType,
      model
    );
    if (baseModelIndex >= 0) {
      result.push({
        field: model.fields[baseModelIndex],
        path: model.fields[baseModelIndex].name,
      });
      // check policy
      if (
        this.configuration.assignmentStrategy ===
        PermissionAssignmentStrategy.FirstMatch
      ) {
        // and return
        return result;
      } else {
        // just splice the found field
        model.fields.splice(baseModelIndex, 1);
      }
    }
    // setup structures
    const fieldsStack: {
      field: DataField;
      path: string;
    }[] = [];
    const alreadyCheckedEntities: string[] = [];
    // add all the current model fields into the stack
    model.fields
      .filter((item: DataField) => {
        return (
          !item.many &&
          (item.mapping == null || item.mapping.associationType !== 'junction')
        );
      })
      .forEach((field) => {
        fieldsStack.push({
          field,
          path: field.name,
        });
      });
    // start checking
    while (fieldsStack.length > 0) {
      // get the first field from the stack (and remove it)
      const currentFieldData = fieldsStack.shift()!;
      const currentField = currentFieldData.field;
      // check if target field has been found
      const fieldFound =
        currentField.type === targetEntityType &&
        !currentField.many &&
        (currentField.mapping == null ||
          currentField.mapping.associationType !== 'junction');
      if (fieldFound) {
        // push to result
        result.push(currentFieldData);
        // check policy
        if (
          this.configuration.assignmentStrategy ===
          PermissionAssignmentStrategy.FirstMatch
        ) {
          // and return
          return result;
        }
      } else {
        if (alreadyCheckedEntities.includes(currentField.type!)) {
          continue;
        }
        // if the field is not found, check if its type refers to a data model
        const dataModel = cloneDeep(
          this.dataConfiguration.getModelDefinition(currentField.type!)
        );
        alreadyCheckedEntities.push(currentField.type!);
        // if it does not, continue
        if (dataModel == null) {
          continue;
        }
        // populate model fields
        this.populateFieldsFromInheritance(dataModel);
        // check next model
        const foundInModel = this.getFieldWithType(targetEntityType, dataModel);
        if (foundInModel) {
          result.push({
            field: foundInModel,
            path: `${currentFieldData.path}/${foundInModel.name}`,
          });
          // check policy
          if (
            this.configuration.assignmentStrategy ===
            PermissionAssignmentStrategy.FirstMatch
          ) {
            // and return
            return result;
          }
        }
        // exclude junctions and 'many' fields
        dataModel.fields = dataModel.fields.filter((item: DataField) => {
          return (
            !item.many &&
            (item.mapping == null ||
              item.mapping.associationType !== 'junction')
          );
        });
        // get nested iteratable fields
        const nestedFields = this.getIteratableFields(dataModel);
        // if there are none, continue
        if (!(Array.isArray(nestedFields) && nestedFields.length > 0)) {
          continue;
        }
        // push all nested fields to the stack
        // note: track the expansion path
        nestedFields.forEach((nestedFieldData) => {
          fieldsStack.push({
            field: nestedFieldData.field,
            path: `${currentFieldData.path}/${nestedFieldData.path}`,
          });
        });
      }
    }
    return result;
  }
}
